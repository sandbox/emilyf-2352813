
--------------------------------------------------------------------------------
Timesheet Additions
--------------------------------------------------------------------------------

Maintainer:  emilyf

This module simply adds some additional fields that work in conjunction with the 
SimpleTimesheet module using Features. This is used by RETN and other community 
media centers for staff to track their time. 

Poject homepage: https://www.drupal.org/sandbox/emilyf/2352813


Installation
------------

The following dependencies need to be installed prior to installing this module:

    - SimpleTimesheet
    - Features (Timesheet Additions are built on Features)
    - Field Permissions (Timesheet Additions assumes you will want to select 
    which user roles can approve/authorize Timesheets)

After you install the above, enable Timesheet Additions from the Module 
Administration page. You will see the new fields show up in the content type.

Now go to admin > people > permissions and locate the permissions for your new 
Timesheet fields. Apply your desired permissions to the roles. For example, if 
you only want your CEO to be able to see and edit the Authorization field, make 
a role (if you haven't already) for your CEO or other similar users, and then 
apply to them the "Create own value for field Authorization", "Edit own value 
for field Authorization", "Edit anyone's value for field Authorization", "View 
own value for field Authorization", "View anyone's value for field 
Authorization". You would not give any other roles any of those permissions. 
If you wanted the user who's timesheet it is to be able to see if their t
imesheet has been authorized, then you will want to give them the ability 
to "view own value for field Authorization". Follow the same decisions for 
the Notes field and Pay Period field depending on your needs.

Usage
-------------

You can now have staff submit time sheets to track their time, and you can 
approve/not approve these time sheets. You can create any views you want 
to assist with this process.

TODO
----

- Add in some views that include unapproved/pending time sheets.
